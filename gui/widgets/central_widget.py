from PySide2.QtWidgets import QWidget, QVBoxLayout, QTextEdit

class CentralWidget(QWidget):
    def __init__(self, parent = None) -> None:
        super().__init__(parent)

        self.cw_layout = QVBoxLayout(self)
        self.text_edit = QTextEdit(self)

        self.cw_layout.addWidget(self.text_edit)

        self.setLayout(self.cw_layout)