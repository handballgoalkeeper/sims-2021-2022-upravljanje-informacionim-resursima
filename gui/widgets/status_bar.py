from PySide2.QtWidgets import QStatusBar

class StatusBar(QStatusBar):
    def __init__(self, parent = None) -> None:
        super().__init__(parent)