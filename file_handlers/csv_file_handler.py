from csv import DictReader
class CSVFileHandler:
    def __init__(self, path = "") -> None:
        self.path = path
        self.csv_file =  open(self.path, "r", encoding="utf-8")
        self.data = DictReader(self.csv_file)

    def login_check(self, user_id = "", user_password = ""):
            for user in self.data:
                if user["user_id"] == user_id and user["user_password"] == user_password:
                    return True
            return False


    def close_file(self):
        self.csv_file.close()
