import sys
from PySide2.QtWidgets import QApplication
from autentication.model import login_model
from gui.main_window import MainWindow
from autentication.ui.login_dialog import LoginDialog
from autentication.controler.login_controler import LoginControler

application = QApplication(sys.argv)

#main_window = MainWindow()
#main_window.show()
login_controler = LoginControler()
login_dialog = LoginDialog(controler=login_controler)
result = login_dialog.exec_()
if result == 1:
    main_window = MainWindow(user = login_dialog.login_model)
    main_window.show()
else:
    sys.exit()

sys.exit(application.exec_())