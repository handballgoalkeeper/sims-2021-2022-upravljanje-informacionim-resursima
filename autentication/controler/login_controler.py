import sys
from autentication.model.login_model import LoginModel
from file_handlers.csv_file_handler import CSVFileHandler
from gui.main_window import MainWindow

class LoginControler:
    def __init__(self) -> None:
        pass
    
    def login(self, model):
        csv_handler = CSVFileHandler("data/users.csv")
        csv_result = csv_handler.login_check(model.user_id, model.user_password)
        csv_handler.close_file()
        if csv_result:
            return True
        return False
        #ako je prijava uspesna pozvati sopstvenu staticku metodu start_app()
        
    def password_change(self, new_password):
        ...
    
    def quit(self):
        ...
    
    def start_app(self, model):
        main_window = MainWindow()
        main_window.show()
    