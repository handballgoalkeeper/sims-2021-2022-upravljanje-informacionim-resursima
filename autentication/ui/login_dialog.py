from PySide2.QtWidgets import QDialog, QLineEdit, QFormLayout, QLabel, QDialogButtonBox
from PySide2.QtGui import QIcon

from autentication.controler.login_controler import LoginControler
from autentication.model.login_model import LoginModel

class LoginDialog(QDialog):
    def __init__(self, parent = None, model = None, controler = None) -> None:
        super().__init__(parent)
        if model is None:
            model = LoginModel()
        self.login_model = model
        self.login_controler = controler

        self.setWindowTitle("Rukovalac informacionim resursima | Log In")
        self.setWindowIcon(QIcon("resources/icons/icons/lock.png"))
        self.resize(400, 100)
        self.form_layout = QFormLayout(self)
        
        self.username_label = QLabel("Username: ")
        self.password_label = QLabel("Password: ")
        self.username_input = QLineEdit()
        self.password_input = QLineEdit()
        self.button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)
        
        if model.user_id is not None:
            self.user_input.setText(self.login_model.user_id)
        if model.user_password is not None:
            self.password_input.setText(self.login_model.user_password)

        self.password_input.setEchoMode(QLineEdit.Password)

        self.form_layout.addRow(self.username_label, self.username_input)
        self.form_layout.addRow(self.password_label, self.password_input)
        self.form_layout.addRow(self.button_box)
    
        self.setLayout(self.form_layout)

    def accept(self):
        self.login_model = LoginModel(self.username_input.text(), self.password_input.text())
        result = self.login_controler.login(self.login_model)
        if result:
            return super().accept()
        else:
            self.password_input.clear()